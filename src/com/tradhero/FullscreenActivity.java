package com.tradhero;

import com.example.tradhero.R;
import com.tradhero.util.SystemUiHider;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;

import abc.notation.Music;
import abc.notation.Tune;
import abc.parser.AbcNode;
import abc.parser.TuneParser;
import abc.parser.TuneParserListenerInterface;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 * 
 * @see SystemUiHider
 */
public class FullscreenActivity extends Activity {
  /**
   * Whether or not the system UI should be auto-hidden after
   * {@link #AUTO_HIDE_DELAY_MILLIS} milliseconds.
   */
  private static final boolean AUTO_HIDE = true;

  /**
   * If {@link #AUTO_HIDE} is set, the number of milliseconds to wait after user
   * interaction before hiding the system UI.
   */
  private static final int AUTO_HIDE_DELAY_MILLIS = 3000;

  /**
   * If set, will toggle the system UI visibility upon interaction. Otherwise,
   * will show the system UI visibility upon interaction.
   */
  private static final boolean TOGGLE_ON_CLICK = false;

  /**
   * The flags to pass to {@link SystemUiHider#getInstance}.
   */
  private static final int HIDER_FLAGS = SystemUiHider.FLAG_HIDE_NAVIGATION;

  /**
   * The instance of the {@link SystemUiHider} for this activity.
   */
  private SystemUiHider mSystemUiHider;

  /**
   * Start button
   */
  private Button startButton;

  private ProgressDialog progressBar;

  private Music music;

  private static final String ABC_LOG_TAG = "ABC";


  /**
   * Sample ABC
   */
  private final static String SIMPLE_ABC = "X:1\n" +
    "T:Simple scale exercise\n" +
    "M:4/4\n" +
    "K:C\n" +
    "ABcdefgabc'\n";


  /**
   * Egan's Polka ABC
   */
  private final static String SAMPLE_ABC = "X: 1\n" +
    "T: Egan's\n" +
    "R: polka\n" +
    "M: 2/4\n" +
    "L: 1/8\n" +
    "K: Dmaj\n" +
    "|:fA BA|fA BA|d2 e>f|ed BA|fA BA| fA BA|d2 e>f| ed d2:|\n" +
    "|:fa f>e| ed BA|d2 e>f| ed BA|fa f>e| ed BA|d2 e>f |ed d2:|\n";
  
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setContentView(R.layout.activity_fullscreen);

    final View controlsView = findViewById(R.id.fullscreen_content_controls);
    final View contentView = findViewById(R.id.fullscreen_content);
    final TextView edit = (TextView) findViewById(R.id.editText1);
    edit.setText(SAMPLE_ABC);
    startButton = (Button) findViewById(R.id.start_button);
    startButton.setOnClickListener(new View.OnClickListener() {
      public void onClick(View v) {
          attemptParse(edit);
      }
    });
    startButton.requestFocus();
    // Set up an instance of SystemUiHider to control the system UI for
    // this activity.
    mSystemUiHider = SystemUiHider.getInstance(this, contentView, HIDER_FLAGS);
    mSystemUiHider.setup();
    mSystemUiHider
        .setOnVisibilityChangeListener(new SystemUiHider.OnVisibilityChangeListener() {
          // Cached values.
          int mControlsHeight;
          int mShortAnimTime;

          @Override
          @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
          public void onVisibilityChange(boolean visible) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
              // If the ViewPropertyAnimator API is available
              // (Honeycomb MR2 and later), use it to animate the
              // in-layout UI controls at the bottom of the
              // screen.
              if (mControlsHeight == 0) {
                mControlsHeight = controlsView.getHeight();
              }
              if (mShortAnimTime == 0) {
                mShortAnimTime = getResources().getInteger(
                    android.R.integer.config_shortAnimTime);
              }
              controlsView.animate()
                  .translationY(visible ? 0 : mControlsHeight)
                  .setDuration(mShortAnimTime);
            } else {
              // If the ViewPropertyAnimator APIs aren't
              // available, simply show or hide the in-layout UI
              // controls.
              controlsView.setVisibility(visible ? View.VISIBLE : View.GONE);
            }

            if (visible && AUTO_HIDE) {
              // Schedule a hide().
              delayedHide(AUTO_HIDE_DELAY_MILLIS);
            }
          }
        });

    // Set up the user interaction to manually show or hide the system UI.
    contentView.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if (TOGGLE_ON_CLICK) {
          mSystemUiHider.toggle();
        } else {
          mSystemUiHider.show();
        }
      }
    });

    // Upon interacting with UI controls, delay any scheduled hide()
    // operations to prevent the jarring behavior of controls going away
    // while interacting with the UI.
    findViewById(R.id.start_button).setOnTouchListener(mDelayHideTouchListener);
    // Hide keyboard on start.
    InputMethodManager imm = (InputMethodManager)getSystemService(
              Context.INPUT_METHOD_SERVICE);
      imm.hideSoftInputFromWindow(edit.getWindowToken(), 0);
  }

    private void attemptParse(TextView edit) {
        String text = edit.getText().toString();
        parseMusic(text);
    }

    @Override
  protected void onPostCreate(Bundle savedInstanceState) {
    super.onPostCreate(savedInstanceState);

    // Trigger the initial hide() shortly after the activity has been
    // created, to briefly hint to the user that UI controls
    // are available.
    delayedHide(100);
  }

  /**
   * Touch listener to use for in-layout UI controls to delay hiding the system
   * UI. This is to prevent the jarring behavior of controls going away while
   * interacting with activity UI.
   */
  View.OnTouchListener mDelayHideTouchListener = new View.OnTouchListener() {
    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
      if (AUTO_HIDE) {
        delayedHide(AUTO_HIDE_DELAY_MILLIS);
      }
      return false;
    }
  };

  Handler mHideHandler = new Handler();
  Runnable mHideRunnable = new Runnable() {
    @Override
    public void run() {
      mSystemUiHider.hide();
    }
  };

  /**
   * Schedules a call to hide() in [delay] milliseconds, canceling any
   * previously scheduled calls.
   */
  private void delayedHide(int delayMillis) {
    mHideHandler.removeCallbacks(mHideRunnable);
    mHideHandler.postDelayed(mHideRunnable, delayMillis);
  }

    /**
     * Parse abc text string into Music.
     *
     * TODO: Handle error in abc parsing.
     * @param text
     */
    private void parseMusic(final String text) {
        // prepare for a progress bar dialog
        progressBar = new ProgressDialog(startButton.getContext());
        progressBar.setCancelable(true);
        progressBar.setMessage("Parsing ABC ...");
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressBar.setIndeterminate(true);
        progressBar.show();
        new Thread(new Runnable() {
            public void run() {
                TuneParser p = new TuneParser();
                p.addListener(new TuneParserListenerInterface() {

                    @Override
                    public void tuneEnd(Tune tune, AbcNode abcRoot) {
                        Log.i(ABC_LOG_TAG, "Tune End ");
                    }

                    @Override
                    public void tuneBegin() {
                        Log.i(ABC_LOG_TAG, "Tune Begin ");
                    }

                    @Override
                    public boolean isBusy() {
                        return false;
                    }

                    @Override
                    public void noTune() {
                        Log.e(ABC_LOG_TAG, "No TUNE");
                    }

                });
                Tune tune = p.parse(text);
                music = tune.getMusicForGraphicalRendition();
                // close the progress bar dialog
                progressBar.dismiss();
                if (music != null) {
                    // Open the full screen activity.
                    Intent i = new Intent(getBaseContext(), MainActivity.class);
                    i.putExtra("com.tradhero.abc", music);
                    startActivity(i);
                    finish();
                } else {
                    // TODO: Add error message.
                }
            }
        }).start();
    }
}
