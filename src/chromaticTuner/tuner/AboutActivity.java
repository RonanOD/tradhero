package chromaticTuner.tuner;

import com.example.tradhero.R;

import android.app.Activity;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.widget.TextView;

public class AboutActivity extends Activity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.about);
		TextView view = (TextView) findViewById(R.id.version_text_view);
		try {
			view.setText("Version: " + getBaseContext().getPackageManager().getPackageInfo(getBaseContext().getPackageName(), 0).versionName);
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onResume()
    {
    	super.onResume();
    }

	@Override
    public void onPause()
    {
    	super.onPause();
    }

}

