package chromaticTuner.tuner;

public interface OnAudioBufferListener {
	public abstract void onAudioBuffer(short[] buffer);
}
