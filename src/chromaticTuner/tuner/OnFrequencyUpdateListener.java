package chromaticTuner.tuner;

public interface OnFrequencyUpdateListener {
	public abstract void onFrequencyUpdate(float frequency);
}
