package chromaticTuner.tuner;

public class RmsMeasurer {
	private double m_sum=0.0;
	private int m_rmsI=0;
	private double m_sample=0;

	public double measureRms(short[] buffer) {
		m_sum = 0;
		for (m_rmsI = 0; m_rmsI < buffer.length; m_rmsI++)
		{
			m_sample = buffer[m_rmsI] / (double) Short.MAX_VALUE;
			m_sum += (m_sample * m_sample);
		}
		return 20 * Math.log10(Math.sqrt(m_sum / (double) buffer.length));
	}
}
