package chromaticTuner.tuner;

public class LowPassFilter {
	private float m_filterDt = 0.0f;
	private float m_filterRC = 0.0f;
	private float m_filterA = 0.0f;
	private int m_filterI = 0;
	private int m_sampleRate=0;
	private float m_filterFrequency=0.0f;
	
	public int getSampleRate()
	{
		return m_sampleRate;
	}
	
	public void setSampleRate(int sampleRate)
	{
		m_sampleRate=sampleRate;
	}
	
	public float getFilterFrequency()
	{
		return m_filterFrequency;
	}
	
	public void setFilterFrequency(float filterFrequency)
	{
		m_filterFrequency=filterFrequency;
	}
	
	public LowPassFilter()
	{
	}
	
	public LowPassFilter(float filterFrequency)
	{
		m_filterFrequency=filterFrequency;
	}
	
	public void filterBuffer(short[] in, short[] out)
			throws Exception {
		if (in.length != out.length || m_filterFrequency <= 0.0)
		{
			throw new Exception();
		}
		m_filterDt = 1 / (float) m_sampleRate;
		m_filterRC = (float) (1 / (2 * Math.PI * m_filterFrequency));
		m_filterA = m_filterDt / (m_filterRC + m_filterDt);
		for (m_filterI = 0; m_filterI < in.length; m_filterI++)
		{
			if (m_filterI == 0)
			{
				out[m_filterI] = in[m_filterI];
			} else
			{
				out[m_filterI] = (short) (m_filterA * in[m_filterI] + (1 - m_filterA)
						* out[m_filterI - 1]);
			}
		}
	}
}
