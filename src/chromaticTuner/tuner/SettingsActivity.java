package chromaticTuner.tuner;

import com.example.tradhero.R;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
//import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
//import android.preference.PreferenceCategory;
import android.preference.PreferenceManager;

public class SettingsActivity extends PreferenceActivity implements OnSharedPreferenceChangeListener {
    public static final String SETTINGS = "chromatic_tuner_settings";
    public static final String OPTION_A_FREQUENCY = "a_frequency";
    private static final String OPTION_A_FREQUENCY_DEFAULT = "440";
    public static final String OPTION_VOLUME_THRESHOLD = "volume_threshold";
    private static final String OPTION_VOLUME_THRESHOLD_DEFAULT = "-48.0";
    public static final String OPTION_DEBUG_VISIBLE = "debug_visible";
    private static final boolean OPTION_DEBUG_VISIBLE_DEFAULT = false;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.layout.settings);

        //CheckBoxPreference debugPref = (CheckBoxPreference) findPreference("debug_visible");
        //this.getPreferenceScreen().removePreference(debugPref);
    }
    
	public static float getAFrequency(Context context) {
        return Float.parseFloat(PreferenceManager.getDefaultSharedPreferences(context).
            getString(OPTION_A_FREQUENCY, OPTION_A_FREQUENCY_DEFAULT));
    }

    public static float getVolumeThreshold(Context context) {
        return Float.parseFloat(PreferenceManager.getDefaultSharedPreferences(context).
            getString(OPTION_VOLUME_THRESHOLD, OPTION_VOLUME_THRESHOLD_DEFAULT));
    }

    public static boolean getDebugVisible(Context context) {
    	return PreferenceManager.getDefaultSharedPreferences(context).
    		getBoolean(OPTION_DEBUG_VISIBLE, OPTION_DEBUG_VISIBLE_DEFAULT);
    }

    @Override
    protected void onResume()
    {
    	super.onResume();
        Preference calibrationPref = (Preference) findPreference("inputVolumeCalibration");
        calibrationPref.setOnPreferenceClickListener(new OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
            	Intent calibrationActivity = new Intent(getBaseContext(),
            			InputVolumeCalibrationActivity.class);
            	startActivity(calibrationActivity);
                return true;
            }
        });
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    	updateSettings();
    }

    @Override
    protected void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
    	updateSettings();
    }

    private void updateSettings()
    {
    	String aFreqString = PreferenceManager.getDefaultSharedPreferences(this).
                getString(OPTION_A_FREQUENCY, OPTION_A_FREQUENCY_DEFAULT);
    	String volumeString = PreferenceManager.getDefaultSharedPreferences(this).
                getString(OPTION_VOLUME_THRESHOLD, OPTION_VOLUME_THRESHOLD_DEFAULT);
    	ListPreference aFreqPreference = (ListPreference)getPreferenceScreen().findPreference(OPTION_A_FREQUENCY);
        ListPreference volumeThresholdPreference = (ListPreference)getPreferenceScreen().findPreference(OPTION_VOLUME_THRESHOLD);
    	aFreqPreference.setSummary(aFreqString + " Hz");
    	aFreqPreference.setValue(aFreqString);
    	volumeThresholdPreference.setSummary(volumeString + " dB");
    	volumeThresholdPreference.setValue(volumeString);
    }
}
