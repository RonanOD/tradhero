package chromaticTuner.tuner;

public interface OnAudioLevelListener {
	public abstract void onAudioLevel(float value);
}
