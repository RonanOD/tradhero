package chromaticTuner.tuner;

public interface OnAudioRecorderInitializeErrorListener {
	public abstract void audioRecorderInitializeError();
}

