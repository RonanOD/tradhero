package chromaticTuner.tuner;

public class NoteCalculator {
	public static final int Gs=0;
	public static final int A=1;
	public static final int As=2;
	public static final int B=3;
	public static final int C=4;
	public static final int Cs=5;
	public static final int D=6;
	public static final int Ds=7;
	public static final int E=8;
	public static final int F=9;
	public static final int Fs=10;
	public static final int G=11;
	
	static public float calculateNoteFromFrequency(float frequency, float aFrequency)
	{
		if (aFrequency==0.0)
		{
			return -1.0f;
		}
		float fPerA = frequency/aFrequency;
		return (float)(12*(Math.log(fPerA)/Math.log(2))+49);
	}
	
	static public float calculateFrequencyFromNote(float note, float aFrequency)
	{
		float freq = (float)(aFrequency * Math.pow(2, (note-49)/12.0));
		return freq;
	}
	
	static public String noteNumberToNoteName(int note)
	{
		int noteInOneOctave = note % 12;
		switch(noteInOneOctave)
		{
			case 0:
				return "G#";
			case 1:
				return "A";
			case 2:
				return "A#";
			case 3:
				return "B";
			case 4:
				return "C";
			case 5:
				return "C#";
			case 6:
				return "D";
			case 7:
				return "D#";
			case 8:
				return "E";
			case 9:
				return "F";
			case 10:
				return "F#";
			case 11:
				return "G";
			default:
				return "N/A";
		}
	}
}
