# TradHero

## Introduction
Traditional Music is often available on-line in the abc format [see The Session site](http://thesession.org) for example.
Most ABC tunes are public domain and free for everyone to use. TradHero is a teaching game for the android platform
that allows the user to enter any abc notation they wish and play along as best they can. It's Guitar Hero For Trad!

## Technology
Uses open source abc parser [abc4j](abc4j.googlecode.com) and expands on a
(chromatic instrument tuner open source project)[http://gitorious.org/chromatic-tuner-for-android].
The tuner reads the notes you play on your instrument and matches them to the abc notes.

Developed as part of [Global Game Jam 2014](http://globalgamejam.org/2014/games/tradhero)

## Installation
This is a standard Android application. Install the Android SDK to put TradHero on your device.